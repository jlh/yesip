#!/usr/bin/env python
# Copyright (c) 2013, Jeremie Le Hen <jeremie@le-hen.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import base64
import urllib2
import sys
import syslog

PROGNAME = "yesip"
VERSION = "1.0"
MAINTAINER = "jeremie@le-hen.org"

GETIP_URL = 'http://ip1.dynupdate.no-ip.com/'
UPDATEIP_URL = 'https://dynupdate.no-ip.com/nic/update'

RESPONSE_CODE = {
    'good' : 'DNS hostname update successful',
    'nochg' : 'IP address is current, no update performed',
    'nohost' : 'Hostname supplied does not exist under specified account',
    'badauth' : 'Invalid username password combination',
    'badagent' : 'Client disabled',
    '!donator' : 'An update request was sent including a feature that is not available to that particular user such as offline options',
    'abuse' : 'Username is blocked due to abuse. Either for not following our update specifications or disabled due to violation of the No-IP terms of service. Our terms of service can be viewed at http://www.noip.com/legal/tos',
    '911' : 'A fatal error on our side such as a database outage. Retry the update no sooner 30 minutes'
}

def usage():
	sys.stderr.write(
"""Usage: %s [-f] <hostname> <user> <password> [ip]

All arguments are self-explanatory.  If "ip" is not specified,
your outgoing WAN IP address will be used.
Option -f disables client-side checking whether the update is needed.
""" % PROGNAME)

def resolve(name):
	import dns.exception
	import dns.resolver
	try:
		answer = dns.resolver.query(name)
	except dns.exception.DNSException:
		return None
        return answer[0].address

def getwanip():
	try:
		return urllib2.urlopen(GETIP_URL).readline().strip()
	except urllib2.URLError:
		return None

def needsupdate(hostname, ip):
	ipdns = resolve(hostname)
	if ipdns is None:
		syslog.syslog(syslog.LOG_WARNING, "Couldn't resolve %s, "
		    "client-side check disabled" % hostname)
		return True
	if ip is None:
		ip = getwanip()
		if ip is None:
			syslog.syslog(syslog.LOG_WARNING, "Couldn't get WAN "
			    "IP address, client-side check disabled")
			return True
	if ip == ipdns:
		return False
	return True

def update(hostname, ip, user, password):
	url = '%s?hostname=%s' % (UPDATEIP_URL, hostname)
	if ip is not None:
		url = url + '&myip=%s' % ip
	req = urllib2.Request(url)
	req.add_header("User-Agent", "%s/%s %s" % \
	    (PROGNAME, VERSION, MAINTAINER))
	req.add_header("Authorization", "Basic %s" % \
	    base64.b64encode("%s:%s" % (user, password)))
	try:
		resp = urllib2.urlopen(req)
	except urllib2.URLError as e:
		syslog.syslog(syslog.LOG_ERR,
		    "Cannot perform request: %s" % e.reason)
		return
	resp = resp.readline().split()
	msg = RESPONSE_CODE[resp[0]]
	if len(resp) > 1:
	    msg = msg + ' (%s)' % resp[1]
	msg = msg + '.'
	syslog.syslog(syslog.LOG_NOTICE, msg)

clientsidecheck = True
del sys.argv[0]
try:
	if sys.argv[0] == "-f":
		clientsidecheck = False
		del sys.argv[0]
	hostname, user, password = sys.argv[0:3]
except:
	usage()
	sys.exit(1)
ip = None
try:
	ip = sys.argv[3]
except:
	pass
if sys.stdin.isatty():
    syslog.openlog(PROGNAME, syslog.LOG_PERROR)
else:
    syslog.openlog(PROGNAME)
if clientsidecheck:
	if needsupdate(hostname, ip):
		update(hostname, ip, user, password)
else:
	update(hostname, ip, user, password)
